package com.gsg.weatherappkata.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gsg.weatherappkata.R;
import com.gsg.weatherappkata.model.view.CityWeatherView;
import com.gsg.weatherappkata.ui.holders.WeatherHolder;

import java.util.List;

/**
 * Adapter for weather recycler
 */
public class WeatherAdapter extends RecyclerView.Adapter<WeatherHolder> {

    private List<CityWeatherView> mWeatherList;

    public WeatherAdapter(List<CityWeatherView> weatherList) {
        this.mWeatherList = weatherList;
    }

    @Override
    public WeatherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeatherHolder(DataBindingUtil.bind(LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false)));
    }

    @Override
    public void onBindViewHolder(WeatherHolder holder, int position) {
        holder.setUpView(mWeatherList.get(position));
    }

    @Override
    public int getItemCount() {
        return mWeatherList.size();
    }

    public void upDateData(List<CityWeatherView> weatherList){
        this.mWeatherList = weatherList;
        notifyDataSetChanged();
    }
}
