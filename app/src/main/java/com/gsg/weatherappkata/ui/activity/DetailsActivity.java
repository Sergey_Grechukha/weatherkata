package com.gsg.weatherappkata.ui.activity;

import android.animation.Animator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;

import com.google.gson.Gson;
import com.gsg.weatherappkata.R;
import com.gsg.weatherappkata.app.WeatherApp;
import com.gsg.weatherappkata.contract.DetailsActivityContract;
import com.gsg.weatherappkata.databinding.ActivityDetailsBinding;
import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.remote.weather.Forecast;
import com.gsg.weatherappkata.model.remote.weather.WeatherList;
import com.gsg.weatherappkata.model.transition.ViewParams;
import com.gsg.weatherappkata.model.view.CityWeatherView;
import com.gsg.weatherappkata.presenter.DetailsActivityPresenter;

import javax.inject.Inject;

import static com.gsg.weatherappkata.ui.activity.MainActivity.TRANSITION_PARAMS;
import static com.gsg.weatherappkata.ui.activity.MainActivity.WEATHER_DETAILS;

/**
 * Weather details activity
 * Shows detailed weather for chosen city
 */
public class DetailsActivity extends AppCompatActivity implements DetailsActivityContract {

    //animation duration
    public static final int DURATION = 700;
    private ActivityDetailsBinding mBinding;
    private WeatherList mCityWeather;
    private Forecast mCurrentForecast;
    private boolean animationFinished;
    @Inject
    protected Repository mRepository;
    private DetailsActivityPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        injectDependencies();

        mPresenter = new DetailsActivityPresenter(mRepository, this);

        setUpViews(getIntent().getExtras());
    }

    private void injectDependencies() {
        ((WeatherApp) getApplication()).getAppComponent().inject(this);
    }

    /**
     * Set data and prepare view for animation
     *
     * @param extras bundles
     */
    private void setUpViews(Bundle extras) {
        if (null == extras) {
            //do nothing if extras are empty
            return;
        }

        if (null != extras.getString(WEATHER_DETAILS) && !extras.getString(WEATHER_DETAILS).isEmpty()) {
            //set data
            mCityWeather = new Gson().fromJson(extras.getString(WEATHER_DETAILS), WeatherList.class);
            CityWeatherView cityWeatherView
                    = new CityWeatherView(mCityWeather.getName(),
                    mCityWeather.getWeather().get(0).getIcon(),
                    mCityWeather.getWeather().get(0).getDescription(),
                    mCityWeather.getMain().getTempMin(),
                    mCityWeather.getMain().getTempMax());
            mBinding.mainInfo.setViewData(cityWeatherView);
            requestDetailedWeather(mCityWeather.getName());
        }
        if (null != extras.getString(TRANSITION_PARAMS) && !extras.getString(TRANSITION_PARAMS).isEmpty()) {
            //set position of view
            ViewParams viewParams = new Gson().fromJson(extras.getString(TRANSITION_PARAMS), ViewParams.class);
            ViewTreeObserver observer = mBinding.mainInfo.getRoot().getViewTreeObserver();
            //wait for view to be built
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mBinding.mainInfo.getRoot().getViewTreeObserver().removeOnPreDrawListener(this);
                    setUpViewPosition(viewParams, mBinding.mainInfo.getRoot());
                    mBinding.mainInfo
                            .getRoot()
                            .setLayoutParams(new PercentRelativeLayout.LayoutParams(viewParams.getWidth(),
                                    viewParams.getHeight()));
                    //start animation
                    animateDetails();
                    return true;
                }
            });
        }
    }

    /**
     * Animate view to the top of the screen
     */
    private void animateDetails() {
        mBinding.mainInfo.getRoot()
                .animate()
                .translationY(10)
                .setDuration(DURATION)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        //set flag
                        animationFinished = true;
                        if (mCurrentForecast != null) {
                            //update data if it has been already received
                            upDateForecast();
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    private void requestDetailedWeather(String name) {
        //Fetch additional weather info for chosen city
        mPresenter.getDetailedWeatherForCity(name);
    }

    /**
     * Set view on the same position as on the previous screen
     *
     * @param params {@link ViewParams}
     * @param view   of interest
     */
    private void setUpViewPosition(ViewParams params, View view) {
        int[] coords = new int[2];
        view.getLocationOnScreen(coords);
        view.setTranslationX(params.getCoordinates()[0] - coords[0]);
        view.setTranslationY(params.getCoordinates()[1] - coords[1]);
    }

    @Override
    public void onWeatherReceived(Forecast weather) {
        //Weather was received
        //save it and show if main view animation is finished
        this.mCurrentForecast = weather;
        if (this.animationFinished) {
            upDateForecast();
        }
    }

    private void upDateForecast() {
        //show temperature chart
        upDateChart();
        //show additional weather info
        upDateWeatherTextFields();
    }

    private void upDateWeatherTextFields() {
        //set formatted data into textViews
        mBinding.pressureTv.setText(String.format(getString(R.string.pressure), mCityWeather.getMain().getPressure()));
        mBinding.humidityTv.setText(String.format(getString(R.string.humidity), mCityWeather.getMain().getHumidity()) + " %");
        mBinding.windTv.setText(mPresenter.areUnitsMetric()
                ? String.format(getString(R.string.wind_metric), mCityWeather.getWind().getSpeed())
                : String.format(getString(R.string.wind_imperial), mCityWeather.getWind().getSpeed()));

    }

    private void upDateChart() {
        //set chart data and show it
        mBinding.tempChart.setData(mPresenter.getLineData(DetailsActivity.this, mCurrentForecast,
                ContextCompat.getColor(DetailsActivity.this, R.color.colorPrimaryDark)));
        mBinding.tempChart.setVisibility(View.VISIBLE);
        mBinding.tempChart.animateY(DURATION);
        mBinding.tempChart.animateX(DURATION);
        mBinding.tempChart.notifyDataSetChanged();
        mBinding.tempChart.invalidate();
    }

    @Override
    public void onError() {
        //something went wrong
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailsActivity.this);
        builder.setTitle(R.string.attention)
                .setMessage(R.string.cannt_show_weather_for_city)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }
}
