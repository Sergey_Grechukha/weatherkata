package com.gsg.weatherappkata.ui.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gsg.weatherappkata.databinding.WeatherItemBinding;
import com.gsg.weatherappkata.model.view.CityWeatherView;

public class WeatherHolder extends RecyclerView.ViewHolder {

    private WeatherItemBinding mBinding;

    public WeatherHolder(WeatherItemBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
    }

    public void setUpView(CityWeatherView data){
        mBinding.setViewData(data);
    }
}
