package com.gsg.weatherappkata.ui.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.gsg.weatherappkata.R;
import com.gsg.weatherappkata.app.WeatherApp;
import com.gsg.weatherappkata.contract.MainActivityContract;
import com.gsg.weatherappkata.databinding.ActivityMainBinding;
import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.view.CityWeatherView;
import com.gsg.weatherappkata.presenter.MainActivityPresenter;
import com.gsg.weatherappkata.ui.adapters.WeatherAdapter;
import com.gsg.weatherappkata.ui.custom.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.gsg.weatherappkata.model.local.LocalStorageHelper.OLD_DATA;
import static com.gsg.weatherappkata.model.remote.ServerHelper.SERVER_RESPONSE_ERROR;
import static com.gsg.weatherappkata.presenter.MainActivityPresenter.NO_DETAIL_FOR_CITY;

/**
 * App main activity
 * Shows weather for list of city
 */
public class MainActivity extends AppCompatActivity implements MainActivityContract {

    //shared transition fields for bundles
    static final String TRANSITION_PARAMS = "detail_view_transition_params";
    static final String WEATHER_DETAILS = "weather_details";
    @Inject
    protected Repository mRepository;
    private ActivityMainBinding mBinding;
    private MainActivityPresenter mPresenter;
    private WeatherAdapter mAdapter;
    private AnimatedVectorDrawableCompat mWaitingDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        injectDependencies();

        init();

        loadWeather();
    }

    private void loadWeather() {
        //hide recycler
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(mBinding.weatherRecycler, View.ALPHA, 1, 0);
        alphaAnimation.setDuration(500).start();
        //show progress image
        mBinding.waitImage.setVisibility(View.VISIBLE);
        mWaitingDrawable.start();
        //start update process
        mPresenter.upDateWeatherList();
    }

    private void init() {
        //instantiate objects
        mPresenter = new MainActivityPresenter(mRepository, this);
        mAdapter = new WeatherAdapter(new ArrayList<>());
        mBinding.weatherRecycler.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        mBinding.weatherRecycler.setAdapter(mAdapter);

        mBinding.weatherRecycler
                .addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this,
                        this::launchDetailsActivity));

        mWaitingDrawable = AnimatedVectorDrawableCompat.create(this, R.drawable.termometer_animated);
        AnimatedVectorDrawableCompat mWaitingDrawable = AnimatedVectorDrawableCompat.create(this, R.drawable.termometer_animated);
        mBinding.waitImage.setImageDrawable(mWaitingDrawable);
    }

    /**
     * Launch DetailsActivity {@link DetailsActivity} with chosen city data
     *
     * @param view     for shared transition
     * @param position city position
     */
    private void launchDetailsActivity(View view, int position) {

        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        //store transition info
        intent.putExtra(TRANSITION_PARAMS,
                mPresenter.getDetailsTransitionParams(DataBindingUtil.bind(view)));
        String details = mPresenter.getCityWeather(position);
        if (null != details) {
            //store weather info
            intent.putExtra(WEATHER_DETAILS, details);
            startActivity(intent);
            overridePendingTransition(0, 0);
        } else {
            //something went wrong
            onError(NO_DETAIL_FOR_CITY);
        }
    }

    private void injectDependencies() {
        ((WeatherApp) getApplication()).getAppComponent().inject(this);
    }

    @Override
    public void onSuccess(List<CityWeatherView> weather) {
        showWeather(weather);
    }

    private void showWeather(List<CityWeatherView> weather) {
        mAdapter.upDateData(weather);
        showWeatherRecycler();
    }

    private void showWeatherRecycler() {
        //hide progress image
        mBinding.waitImage.setVisibility(View.GONE);
        mWaitingDrawable.stop();

        //show recycler
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(mBinding.weatherRecycler, View.ALPHA, 0, 1);
        alphaAnimation.setDuration(500).start();
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage;
        switch (errorCode) {
            case SERVER_RESPONSE_ERROR:
                errorMessage = getString(R.string.failed_to_load);
                break;
            case OLD_DATA:
                errorMessage = getString(R.string.old_data);
                break;
            case NO_DETAIL_FOR_CITY:
                errorMessage = getString(R.string.cannt_show_weather_for_city);
                break;
            default:
                errorMessage = getString(R.string.unsupported_error);
        }
        mWaitingDrawable.stop();
        showErrorDialog(errorMessage);
    }

    private void showErrorDialog(String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attention)
                .setMessage(errorMessage)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
                    showWeatherRecycler();
                    dialog.dismiss();
                })
                .setPositiveButton(getString(R.string.try_again), (dialog, which) -> {
                    dialog.dismiss();
                    loadWeather();
                })
                .create()
                .show();
    }
}
