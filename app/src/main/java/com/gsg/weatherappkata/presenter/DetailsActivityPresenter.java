package com.gsg.weatherappkata.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.gsg.weatherappkata.R;
import com.gsg.weatherappkata.contract.DetailsActivityContract;
import com.gsg.weatherappkata.helpers.AppSchedulers;
import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.remote.weather.Forecast;
import com.gsg.weatherappkata.model.remote.weather.WeatherList;
import com.gsg.weatherappkata.ui.activity.DetailsActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * {@link DetailsActivity} presenter
 * Use it to manipulate with data (get/save/process)
 */
public class DetailsActivityPresenter {

    private static final String TAG = DetailsActivityPresenter.class.getSimpleName();
    private Repository mRepository;
    private DetailsActivityContract mCallBack;

    public DetailsActivityPresenter(Repository mRepository, DetailsActivityContract mCallBack) {
        this.mRepository = mRepository;
        this.mCallBack = mCallBack;
    }

    /**
     * Get weather for chosen city
     * There is no caching policy for this method
     * This method fires of the methods of contract {@link DetailsActivityContract} as result
     * @param city of interest
     */
    public void getDetailedWeatherForCity(String city){
        mRepository.getWeatherForCity(city)
                .subscribeOn(AppSchedulers.io())
                .observeOn(AppSchedulers.mainThread())
                .subscribe(weather -> {
                    Log.d(TAG, "getDetailedWeatherForCity: " + new Gson().toJson(weather));
                    if (null != weather) {
                        //everything is fine show results in activity
                        mCallBack.onWeatherReceived(weather);
                    }
                }, error -> {
                    //oups error happened
                    Log.d(TAG, "getDetailedWeatherForCity: error = " + error.getMessage());
                    mCallBack.onError();
                });
    }

    /**
     * Return LineData for temperature chart
     * @param result - forecast for current city
     * @param dataColor - color of chart
     * @return - LineData
     */
    @NonNull
    public LineData getLineData(Context context, Forecast result, int dataColor) {
        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> yValsTemp = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM", Locale.getDefault());
        for (WeatherList weatherList : result.getList()) {
            xVals.add(sdf.format(weatherList.getDt()));
            yValsTemp.add(new Entry(weatherList.getMain().getTemp(), xVals.size() - 1));
        }

        LineDataSet setTemperature = setUpLineSet(new LineDataSet(yValsTemp, context.getString(R.string.temp)),
                dataColor);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(setTemperature);

        return new LineData(xVals, dataSets);
    }

    /**
     * Set up chart appearance
     * @param set1 data set
     * @param mainColor colors
     * @return {@link LineDataSet}
     */
    private LineDataSet setUpLineSet(LineDataSet set1, int mainColor) {
        set1.setLineWidth(2f);
        set1.setCircleRadius(3f);
        set1.setColor(mainColor);
        set1.setDrawValues(false);
        set1.setDrawFilled(false);
        set1.setCircleColor(mainColor);
        set1.setCircleColorHole(mainColor);
        set1.setDrawHighlightIndicators(true);
        set1.setDrawVerticalHighlightIndicator(true);
        return set1;
    }

    /**
     * Check current units type
     * @return true for metric, false for imperial
     */
    public boolean areUnitsMetric() {
        return mRepository.getMetricUnits();
    }
}
