package com.gsg.weatherappkata.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.gsg.weatherappkata.contract.MainActivityContract;
import com.gsg.weatherappkata.databinding.WeatherItemBinding;
import com.gsg.weatherappkata.helpers.AppSchedulers;
import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;
import com.gsg.weatherappkata.model.remote.weather.WeatherList;
import com.gsg.weatherappkata.model.transition.ViewParams;
import com.gsg.weatherappkata.model.view.CityWeatherView;
import com.gsg.weatherappkata.ui.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;

import rx.Single;

import static com.gsg.weatherappkata.model.remote.ServerHelper.RESPONSE_OK;

/**
 * {@link MainActivity} presenter
 * Use it to manipulate with data (get/save/process)
 */
public class MainActivityPresenter {

    private static final String TAG = MainActivityPresenter.class.getSimpleName();
    //error codes
    public static final int UNSUPPORTED_ERROR = 406;
    public static final int NO_DETAIL_FOR_CITY = 407;
    private Repository mRepository;
    private MainActivityContract mCallBack;
    private MultiCityWeather mCurrentWeather;

    public MainActivityPresenter(Repository mRepository, MainActivityContract mCallBack) {
        this.mRepository = mRepository;
        this.mCallBack = mCallBack;
    }

    /**
     * Get list of for list of cities
     * For now method returns weather for hardcoded list
     * Fires one of the contract {@link MainActivityContract} methods as a result
     */
    public void upDateWeatherList(){
        Single.fromCallable(() -> mRepository.getWeatherForCityList(getCityList()))
        .subscribeOn(AppSchedulers.io())
        .observeOn(AppSchedulers.mainThread())
        .subscribe(weather -> {
            Log.d(TAG, "upDateWeatherList: " + new Gson().toJson(weather));
            if (weather != null && weather.getError() == RESPONSE_OK) {
                //everything is ok
                //memorise result and show it
                mCurrentWeather = weather;
                mCallBack.onSuccess(getViewWeatherList(weather));
            } else {
                //something went wrong
                //show error
                mCallBack.onError(weather != null ? weather.getError() : UNSUPPORTED_ERROR);
            }
        }, error -> {
            //something went really wrong
            //show error
            Log.d(TAG, "upDateWeatherList: error = " + error.getMessage());
            mCallBack.onError(UNSUPPORTED_ERROR);
        });
    }

    /**
     * Transform server data into viewDataObject
     * @param weather {@link MultiCityWeather}
     * @return {@link List<CityWeatherView>}
     */
    private List<CityWeatherView>  getViewWeatherList(MultiCityWeather weather) {
        List<CityWeatherView> cityWeatherViews = new ArrayList<>();
        for (WeatherList weatherItem : weather.getList()) {
            String cityName = weatherItem.getName();
            String url = weatherItem.getWeather().get(0).getIcon();
            String desc = weatherItem.getWeather().get(0).getDescription();
            float tempMin = weatherItem.getMain().getTempMin();
            float tempMax =  weatherItem.getMain().getTempMax();
            cityWeatherViews.add(new CityWeatherView(cityName, url, desc, tempMin, tempMax));
        }
        return cityWeatherViews;
    }

    /**
     * Get shared views params
     * @param binding view to be shared
     * @return json string {@link ViewParams}
     */
    public String getDetailsTransitionParams(WeatherItemBinding binding) {

        ViewParams cityViewParams = new ViewParams();
        int[] cityViewCoords = new int[2];
        binding.getRoot().getLocationOnScreen(cityViewCoords);
        cityViewParams.setCoordinates(cityViewCoords);
        cityViewParams.setWidth(binding.getRoot().getWidth());
        cityViewParams.setHeight(binding.getRoot().getHeight());

        return new Gson().toJson(cityViewParams);

    }

    /**
     * Hardcode for city list
     *
     * @return
     */
    public List<String> getCityList() {
        List<String> cities = new ArrayList<>();
        //Helsinki
        cities.add("658225");
        //Kiev
        cities.add("703448");
        //Madrid
        cities.add("3117735");
        //Kherson
        cities.add("706448");
        //New York City
        cities.add("5128581");
        //London
        cities.add("4298960");
        //Tokyo
        cities.add("1850147");
        //Sidney
        cities.add("5172078");
        //Mexico
        cities.add("4398103");
        //Rome
        cities.add("4219762");
        return cities;
    }

    /**
     * Fetch city details from big list
     * @param position city position in big list
     * @return json String {@link WeatherList}
     */
    public String getCityWeather(int position) {
        if (mCurrentWeather != null) {
            return new Gson().toJson(mCurrentWeather.getList().get(position));
        }
        return null;
    }
}
