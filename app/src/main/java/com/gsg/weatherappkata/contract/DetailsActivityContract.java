package com.gsg.weatherappkata.contract;

import com.gsg.weatherappkata.model.remote.weather.Forecast;

public interface DetailsActivityContract {

    /**
     * Detailed weather for selected city was received
     * and can be shown
     * @param weather {@link Forecast}
     */
    void onWeatherReceived(Forecast weather);

    /**
     * Request for current city weather failed
     */
    void onError();
}
