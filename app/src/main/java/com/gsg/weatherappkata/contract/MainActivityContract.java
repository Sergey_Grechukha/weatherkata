package com.gsg.weatherappkata.contract;

import com.gsg.weatherappkata.model.view.CityWeatherView;

import java.util.List;

public interface MainActivityContract {

    /**
     * List of weathers for city list was received
     * and can be shown
     * @param weather {@link List<CityWeatherView>}
     */
    void onSuccess(List<CityWeatherView> weather);

    /**
     * There were problems with weather request:

     * @param errorCode
     * errorCode = SERVER_RESPONSE_ERROR = 404 no weather available
     * errorCode = OLD_DATA = 405 server failed, weather fetched from local storage
     */
    void onError(int errorCode);
}
