package com.gsg.weatherappkata.app;

import android.app.Application;

import com.gsg.weatherappkata.di.AppComponent;
import com.gsg.weatherappkata.di.AppModule;
import com.gsg.weatherappkata.di.DaggerAppComponent;

public class WeatherApp extends Application {

    protected AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public void setAppComponent(AppComponent component) {
        this.mAppComponent = component;
    }
}
