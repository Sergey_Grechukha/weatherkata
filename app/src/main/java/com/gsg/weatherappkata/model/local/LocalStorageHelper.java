package com.gsg.weatherappkata.model.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.gsg.weatherappkata.R;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;

import java.util.List;
import java.util.Locale;

import static com.gsg.weatherappkata.model.remote.ServerHelper.SERVER_RESPONSE_ERROR;

/**
 * Helper for local data storage
 * Cache and get (if problems with server) all received data and settings here
 */
public class LocalStorageHelper {

    //error code
    public static final int OLD_DATA = 405;
    //Preference fields names
    private static final String UNITS = "settings_units";
    private static final String LANG = "settings_lang";
    private static final String WEATHER_LIST = "settings_weather_list";
    private static final String LAST_UPDATE = "settings_last_update";
    private static final String CITIES_WEATHER = "settings_cities_weather";
    private SharedPreferences mPreference;
    private SharedPreferences.Editor mEditor;

    public LocalStorageHelper(Context context) {
        this.mPreference = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        this.mEditor = mPreference.edit();
    }

    /**
     * Get units for response data
     * @return metric (default) or imperial
     */
    public String getUnits() {
        return mPreference.getString(UNITS, "metric");
    }

    /**
     * Set language for response data
     * @param isMetric true for metric
     */
    public void setUnitsMetric(boolean isMetric) {
        mEditor.putString(UNITS, isMetric ? "metric" : "imperial");
    }

    /**
     * Get language for response data
     * @return language type (device language by default)
     * @see <a href="http://openweathermap.org/current">Examples</a>
     */
    public String getLang() {
        return mPreference.getString(LANG, Locale.getDefault().getDisplayLanguage());
    }

    /**
     * Set language for server responses
     * @param lang
     * @see <a href="http://openweathermap.org/current">Examples</a>
     */
    public void setLang(String lang){
        if (lang != null && !lang.isEmpty()) {
            mEditor.putString(LANG, lang);
        }
    }

    /**
     * Get last known weather for last fetched city list
     * @return {@link MultiCityWeather}. May return empty object but with
     * error code SERVER_RESPONSE_ERROR = 404 this means "oups - no data"
     * Otherwise it will return object with data and error code OLD_DATA = 405
     */
    public MultiCityWeather getLastKnownCitiesWeather() {
        String json = mPreference.getString(WEATHER_LIST, null);
        MultiCityWeather result = new MultiCityWeather();
        if (json != null && !json.isEmpty()) {
            result = new Gson().fromJson(json, MultiCityWeather.class);
            result.setError(OLD_DATA);
            return result;
        } else {
            result.setError(SERVER_RESPONSE_ERROR);
            return result;
        }
    }

    /**
     * Save cities weather in local storage
     * @param weather {@link MultiCityWeather}
     */
    public void saveLastKnownCitiesWeather(MultiCityWeather weather) {
        mEditor.putString(CITIES_WEATHER, new Gson().toJson(weather));
    }

    /**
     * Last time server was successfully called
     * @return time in millis
     */
    public long getLastUpDateTime() {
        return mPreference.getLong(LAST_UPDATE, 0);
    }

    /**
     * Save time of successful request
     */
    public void saveSuccessRequestTime(){
        mEditor.putLong(LAST_UPDATE, System.currentTimeMillis());
    }
}
