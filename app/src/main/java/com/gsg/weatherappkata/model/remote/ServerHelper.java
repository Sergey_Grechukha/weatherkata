package com.gsg.weatherappkata.model.remote;

import android.util.Log;

import com.gsg.weatherappkata.model.remote.weather.Forecast;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Single;

/**
 * Helper for net communication
 * All new data can be received here
 */
public class ServerHelper {

    private static final String TAG = ServerHelper.class.getSimpleName();
    private static final String BASE_URL = "http://api.openweathermap.org";
    //error codes
    public static final int SERVER_RESPONSE_ERROR = 404;
    public static final int RESPONSE_OK = 200;
    private WeatherApi mApi;

    /**
     * Return serverApi instance with all api end points methods
     */
    public ServerHelper() {
        this.mApi = getServerApi();
    }

    /**
     * Set up serverApi instance
     */
    protected WeatherApi getServerApi() {
        Retrofit mRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        return mRetrofit.create(WeatherApi.class);
    }

    /**
     * Make a request to fetch weather for given list of cities
     * @param citiesList list of cities ids
     * @param units units (metric/imperial) of incoming data
     * @param lang language of incoming data
     * @return {@link MultiCityWeather}
     *          returns object with error code if something went wrong:
     *          errorCode = SERVER_RESPONSE_ERROR = 404 no weather available
     *          errorCode = OLD_DATA = 405 server failed, weather fetched from local storage
     *          errorCode = RESPONSE_OK = 200 everything is OK
     */
    public MultiCityWeather getWeatherForCities(List<String> citiesList, String units, String lang) {
        MultiCityWeather weatherResponse = new MultiCityWeather();
        Call<MultiCityWeather> request = mApi.getWeatherForCityList(buildCitiesIdListString(citiesList), units, lang);
        try {
            Response<MultiCityWeather> response = request.execute();
            if (response.isSuccessful()) {
                //good answer
                //cache and show data
                weatherResponse = response.body();
                weatherResponse.setError(RESPONSE_OK);
                return response.body();
            } else {
                //error
                //set error code
                Log.e(TAG, "getWeatherForCities server failed to respond:" + response.errorBody());
                weatherResponse.setError(SERVER_RESPONSE_ERROR);
                return weatherResponse;
            }
        } catch (IOException e) {
            //unsupported error
            //set error code
            Log.e(TAG, "getWeatherForCities server failed to respond:" + e.getMessage());
            weatherResponse.setError(SERVER_RESPONSE_ERROR);
            return weatherResponse;
        }

    }

    /**
     * Transforms list of ids into string of ids
     * @param cities - list of cities ids
     * @return - string
     */
    private String buildCitiesIdListString(List<String> cities) {

        if (cities != null && !cities.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < cities.size(); i++) {
                sb.append(cities.get(i));
                if (i != cities.size() - 1) {
                    sb.append(",");
                }
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * Fetch weather for defined city
     * @param city city of interest
     * @param units response data units (metric/imperial)
     * @return {@link Single<Forecast>}
     */
    public Single<Forecast> getWeatherForCity(String city, String units){
        return mApi.loadForecast(city, units);
    }
}
