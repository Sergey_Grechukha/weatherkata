package com.gsg.weatherappkata.model.view;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsg.weatherappkata.R;
import com.squareup.picasso.Picasso;

/**
 * Recycler item viewDataObject
 */
public class CityWeatherView {

    //base url for images
    private static final String IMAGE_BASE_URL = "http://openweathermap.org/img/w/";
    //image file type suffix
    private static final String WEATHER_FILE_TYPE = ".png";
    private String cityName;
    private String weatherUrl;
    private String weatherDesc;
    private float tempMin;
    private float tempMax;


    public CityWeatherView(String cityName, String weatherUrl, String weatherDesc, float tempMin, float tempMax) {
        this.cityName = cityName;
        this.weatherUrl = weatherUrl;
        this.weatherDesc = weatherDesc;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
    }

    public String getCityName() {
        return cityName;
    }

    public String getWeatherUrl() {
        return weatherUrl;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public float getTempMin() {
        return tempMin;
    }

    public float getTempMax() {
        return tempMax;
    }

    /**
     * View binding auto load image resource
     * @param view target view
     * @param weatherUrl resource path (use with IMAGE_BASE_URL and image suffix
     */
    @BindingAdapter({"bind:weatherUrl"})
    public static void loadImage(ImageView view, String weatherUrl) {
        Picasso.with(view.getContext())
                .load(IMAGE_BASE_URL + weatherUrl + WEATHER_FILE_TYPE)
                .into(view);
    }

    /**
     * View binding format temp TextView
     * @param view target TextView
     * @param minTemp minimal temperature
     * @param maxTemp maximal temperature
     */
    @BindingAdapter({"bind:minTemp", "bind:maxTemp"})
    public static void getTempDesc(TextView view, float minTemp, float maxTemp){
        String descTemp = String.format(view.getContext().getString(R.string.format_temperature), minTemp)
                + "\n" + String.format(view.getContext().getString(R.string.format_temperature), maxTemp);
        view.setText(descTemp);
    }
}
