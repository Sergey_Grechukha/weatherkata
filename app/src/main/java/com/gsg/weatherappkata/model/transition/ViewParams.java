package com.gsg.weatherappkata.model.transition;

/**
 * Shared transition helper object
 */
public class ViewParams {

    //shared view width
    private int width;
    //shared view height
    private int height;
    //shared view coords
    private int[] coordinates;

    public int[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(int[] coordinates) {
        this.coordinates = coordinates;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
