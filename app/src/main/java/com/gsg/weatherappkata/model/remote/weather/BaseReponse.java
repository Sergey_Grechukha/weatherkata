package com.gsg.weatherappkata.model.remote.weather;

/**
 * Created by a1 on 9/30/16.
 */

public class BaseReponse {

    int error;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
