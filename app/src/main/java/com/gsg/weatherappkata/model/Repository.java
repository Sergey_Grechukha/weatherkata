package com.gsg.weatherappkata.model;

import com.gsg.weatherappkata.model.local.LocalStorageHelper;
import com.gsg.weatherappkata.model.remote.ServerHelper;
import com.gsg.weatherappkata.model.remote.weather.Forecast;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;

import java.util.List;

import rx.Single;

import static com.gsg.weatherappkata.model.remote.ServerHelper.RESPONSE_OK;

/**
 * Main acces poin to all app data
 * Use it in presenters to access any data.
 * Repository has access to local and remote data resources
 * and provides caching and access logic
 */
public class Repository {

    private static final String TAG = Repository.class.getSimpleName();
    private static final long UPDATE_PERIOD = 1000 * 60 * 5;
    private LocalStorageHelper mLocalStorage;
    private ServerHelper mRemoteStorage;

    /**
     * Default constructor to be used in tests
     */
    public Repository(){}

    /**
     * Main constructor
     * @param localStorage instance of local storage
     * @param remoteStorage instance of remote storage
     */
    public Repository(LocalStorageHelper localStorage, ServerHelper remoteStorage) {
        mLocalStorage = localStorage;
        mRemoteStorage = remoteStorage;
    }

    /**
     * Get weather from server.
     * If server fails to respond it wil fetch weather from local storage.
     * @param citiesList list of cities codes (see link below)
     * @return {@link MultiCityWeather} May return empty object but with error code
     * error code = RESPONSE_OK = 200 - everything is OK
     * error code = SERVER_RESPONSE_ERROR = 404 - everything is Bad - there is no data
     * error code = OLD_DATA = 405 - server failed, there is old data
     * @see <a href="http://openweathermap.org/help/city_list.txt">List of cities</a>
     */
    public MultiCityWeather getWeatherForCityList(List<String> citiesList) {
        MultiCityWeather result;
        if (!needUpdate()) {
            //do not fetch weather from server
            result = mLocalStorage.getLastKnownCitiesWeather();
            result.setError(RESPONSE_OK);
            return result;
        }
        result = mRemoteStorage.getWeatherForCities(citiesList, mLocalStorage.getUnits(), mLocalStorage.getLang());
        if (result.getError() == RESPONSE_OK) {
            //everything is ok
            mLocalStorage.saveLastKnownCitiesWeather(result);
            mLocalStorage.saveSuccessRequestTime();
            return result;
        } else {
            //trying to fetch old data from local storage
            return mLocalStorage.getLastKnownCitiesWeather();
        }
    }

    /**
     * Check time offset
     * @return true if last upDate was more then UPDATE_PERIOD ago
     */
    private boolean needUpdate() {
        return (System.currentTimeMillis() - mLocalStorage.getLastUpDateTime()) > UPDATE_PERIOD;
    }

    /**
     * Get weather for a single city
     * @param city of interest
     * @return {@link Single<Forecast>}
     */
    public Single<Forecast> getWeatherForCity(String city){
        return mRemoteStorage.getWeatherForCity(city, mLocalStorage.getUnits());
    }

    /**
     * Get current units type
     * @return true if metric units are stored as default
     */
    public boolean getMetricUnits() {
        return mLocalStorage.getUnits().equals("metric");
    }
}


