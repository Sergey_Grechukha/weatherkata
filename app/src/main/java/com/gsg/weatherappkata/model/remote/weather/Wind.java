
package com.gsg.weatherappkata.model.remote.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed")
    @Expose
    private float speed;
    @SerializedName("deg")
    @Expose
    private float deg;
    @SerializedName("var_beg")
    @Expose
    private float varBeg;
    @SerializedName("var_end")
    @Expose
    private float varEnd;

    /**
     * 
     * @return
     *     The speed
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * 
     * @param speed
     *     The speed
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * 
     * @return
     *     The deg
     */
    public float getDeg() {
        return deg;
    }

    /**
     * 
     * @param deg
     *     The deg
     */
    public void setDeg(float deg) {
        this.deg = deg;
    }

    /**
     * 
     * @return
     *     The varBeg
     */
    public float getVarBeg() {
        return varBeg;
    }

    /**
     * 
     * @param varBeg
     *     The var_beg
     */
    public void setVarBeg(Integer varBeg) {
        this.varBeg = varBeg;
    }

    /**
     * 
     * @return
     *     The varEnd
     */
    public float getVarEnd() {
        return varEnd;
    }

    /**
     * 
     * @param varEnd
     *     The var_end
     */
    public void setVarEnd(float varEnd) {
        this.varEnd = varEnd;
    }

}
