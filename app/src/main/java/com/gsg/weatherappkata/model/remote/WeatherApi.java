package com.gsg.weatherappkata.model.remote;

import com.gsg.weatherappkata.model.remote.weather.Forecast;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Single;

public interface WeatherApi {

    /*
    example request for multi city request:
    http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric&appid=c3d6f87a866d27f40e56fe4ef5bdf656
     */

    /**
     * Return list of weather for list of cities
     * @param cityIdList - list of cities ids separated by coma
     * @param units - units
     * @param lang - language
     * @return - {@link MultiCityWeather}
     */
    @GET("/data/2.5/group?APPID=c3d6f87a866d27f40e56fe4ef5bdf656")
    Call<MultiCityWeather> getWeatherForCityList(
            @Query("id") String cityIdList,
            @Query("units") String units,
            @Query("lang") String lang);

    /**
     * get forecast for chosen city
     * @param city - chosen city
     * @param units - units
     * @return - {@link Forecast}
     */
    //5/3 forecast weather request
    @GET("/data/2.5/forecast?APPID=c3d6f87a866d27f40e56fe4ef5bdf656&lang=ru&mode=json")
    Single<Forecast> loadForecast(
            @Query("q") String city,
            @Query("units") String units);
}
