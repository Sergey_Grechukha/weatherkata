package com.gsg.weatherappkata.di;

import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.ui.activity.DetailsActivity;
import com.gsg.weatherappkata.ui.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainActivity activity);

    Repository getRepository();

    void inject(DetailsActivity activity);
}
