package com.gsg.weatherappkata.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.local.LocalStorageHelper;
import com.gsg.weatherappkata.model.remote.ServerHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return mContext;
    }

    @Singleton
    @Provides
    public Repository provideRepository() {
        return getRepository();
    }

    @NonNull
    protected Repository getRepository() {
        return new Repository(new LocalStorageHelper(mContext), new ServerHelper());
    }
}
