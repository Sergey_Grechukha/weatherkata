package com.gsg.weatherappkata.rules;

import android.content.Context;

import com.gsg.weatherappkata.app.WeatherApp;
import com.gsg.weatherappkata.di.AppComponent;
import com.gsg.weatherappkata.di.DaggerTestDataComponent;
import com.gsg.weatherappkata.di.TestDataComponent;
import com.gsg.weatherappkata.di.TestDataModule;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Substitute here appComponent {@link AppComponent} with
 * mock one {@link TestDataComponent}
 */
public class MyTestRule implements TestRule {

    private final TestDataComponent mTestComponent;
    private final Context mContext;

    public MyTestRule(Context context) {
        mContext = context;

        mTestComponent = DaggerTestDataComponent.builder().testDataModule(new TestDataModule(mContext)).build();

    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                WeatherApp application = (WeatherApp) mContext.getApplicationContext();
                // Set the TestComponent before the test runs
                application.setAppComponent(mTestComponent);
                base.evaluate();
                // Clears the component once the test finishes so it would use the default one.
                application.setAppComponent(null);
            }
        };
    }
}
