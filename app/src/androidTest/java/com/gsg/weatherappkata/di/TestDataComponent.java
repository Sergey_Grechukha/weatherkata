package com.gsg.weatherappkata.di;

import com.gsg.weatherappkata.model.Repository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = TestDataModule.class)
public interface TestDataComponent extends AppComponent{

    @Singleton
    Repository getRepository();
}
