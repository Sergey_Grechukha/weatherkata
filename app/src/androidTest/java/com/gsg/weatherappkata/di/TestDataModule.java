package com.gsg.weatherappkata.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.gsg.weatherappkata.mocks.MockRepository;
import com.gsg.weatherappkata.model.Repository;

import dagger.Module;

@Module
public class TestDataModule extends AppModule{

    public TestDataModule(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected Repository getRepository() {
        //substitute mock repository instead of real one
        return new MockRepository();
    }
}
