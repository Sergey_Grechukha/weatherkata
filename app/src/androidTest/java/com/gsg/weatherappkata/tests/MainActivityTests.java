package com.gsg.weatherappkata.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.gsg.weatherappkata.R;
import com.gsg.weatherappkata.app.WeatherApp;
import com.gsg.weatherappkata.mocks.MockRepository;
import com.gsg.weatherappkata.rules.MyTestRule;
import com.gsg.weatherappkata.ui.activity.DetailsActivity;
import com.gsg.weatherappkata.ui.activity.MainActivity;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.gsg.weatherappkata.model.remote.ServerHelper.SERVER_RESPONSE_ERROR;

/**
 * {@link MainActivity} tests
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTests {

    //Component substitution rule
    private final MyTestRule component =
            new MyTestRule(InstrumentationRegistry.getTargetContext());
    //Activity rule
    private final ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, false, false);

    //Combine rules
    @Rule
    public TestRule chain = RuleChain.outerRule(component).around(activityRule);

    private MockRepository mRepository;

    @Before
    public void setUp(){
        //Get MockRepository instance
        WeatherApp mApp = (WeatherApp) getInstrumentation().getTargetContext().getApplicationContext();
        mRepository = (MockRepository) mApp.getAppComponent().getRepository();
    }

    public void launchActivity(){
        activityRule.launchActivity(null);
    }

    /**
     * Everything went wrong
     * Activity must show alert indicating bad situation
     */
    @Test
    public void failAlertShouldAppear() {
        //set up desirable response
        mRepository.setResult(SERVER_RESPONSE_ERROR);
        launchActivity();
        //check result
        onView(withText(R.string.attention)).check(matches(isDisplayed()));
    }


}
