package com.gsg.weatherappkata.mocks;

import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;

import java.util.List;

import static com.gsg.weatherappkata.model.local.LocalStorageHelper.OLD_DATA;
import static com.gsg.weatherappkata.model.remote.ServerHelper.RESPONSE_OK;
import static com.gsg.weatherappkata.model.remote.ServerHelper.SERVER_RESPONSE_ERROR;

/**
 * Mock repository
 * Set up it for various cases and make it respond as you wish
 */
public class MockRepository extends Repository {

    private int result;

    public MockRepository() {
        super();
    }

    public void setResult(int responseType){
        this.result = responseType;
    }

    /**
     * Overriding original method this one will return mock data
     * depending on RESULT one passes here
     * @param citiesList pass empty Array
     * @return {@link MultiCityWeather} Do not forget to set up this object before returning it
     */
    @Override
    public MultiCityWeather getWeatherForCityList(List<String> citiesList) {
        switch (result){
            //everything is fine case
            case RESPONSE_OK: return null;
            //old data case
            case OLD_DATA: return null;
            //some errors case
            default:
                MultiCityWeather errorResponse = new MultiCityWeather();
                errorResponse.setError(SERVER_RESPONSE_ERROR);
                return errorResponse;
        }
    }
}
