package com.gsg.weatherappkata;

import com.gsg.weatherappkata.model.Repository;
import com.gsg.weatherappkata.model.local.LocalStorageHelper;
import com.gsg.weatherappkata.model.remote.ServerHelper;
import com.gsg.weatherappkata.model.remote.weather.MultiCityWeather;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

public class RepositoryTests {

    @Mock
    private LocalStorageHelper mLocalStorage;
    @Mock private ServerHelper mServerHelper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Repository must not fetch server data within UPDATE_PERIOD but
     * teke it from local storage
     * @throws Exception
     */
    @Test
    public void repositoryShouldNotFetchIfTime() throws Exception {
        when(mLocalStorage.getLastUpDateTime()).thenReturn(System.currentTimeMillis());
        when(mLocalStorage.getLastKnownCitiesWeather()).thenReturn(new MultiCityWeather());
        Repository repo = new Repository(mLocalStorage, mServerHelper);
        repo.getWeatherForCityList(new ArrayList<>());
        verify(mServerHelper, times(0)).getWeatherForCities(any(), anyString(), anyString());
        verify(mLocalStorage, times(1)).getLastKnownCitiesWeather();
    }
}
